import api from './api'

export function getRooms () {
  return api.get('/rooms')
}
